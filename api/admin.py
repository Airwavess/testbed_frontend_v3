from django.contrib import admin

from .models import Dsal, Report


class DsalAdmin(admin.ModelAdmin):
    list_display = ('dsal_filename', 'dsal_content')


class ReportAdmin(admin.ModelAdmin):
    list_display = ('dsal', 'report_name', 'report_content')


admin.site.register(Dsal, DsalAdmin)
admin.site.register(Report, ReportAdmin)
