import codecs
import json
import os

from django.contrib.auth.models import User
from django.http import HttpResponse
import pandas as pd
import redis
from redis.exceptions import ConnectionError

from rest_framework import generics, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Dsal, Report
from .serializers import (
    UserSerializer,
    DsalSerializer,
    EmulationSerializer,
    ReportSerializer
)
from api.emulation.task_manager import TaskManager
from .dsal import convert_dsal_to_datasetting

task_manager = TaskManager()


class UserViewSet(generics.ListAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    # permission_classes = (IsAuthenticated,)


class DsalList(generics.ListCreateAPIView):
    queryset = Dsal.objects.all()
    serializer_class = DsalSerializer
    permission_classes = (IsAuthenticated,)


class DsalItem(generics.RetrieveUpdateDestroyAPIView):
    queryset = Dsal.objects.all()
    serializer_class = DsalSerializer
    permission_classes = (IsAuthenticated,)


class DataSettings(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        try:
            data_setting = convert_dsal_to_datasetting(request.data['dsal_content'])
            return Response({'data_setting': data_setting})
        except Exception as e:
            return Response({'error': str(e)})


class DataSetting(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    def get_object(self, dsal_filename):
        return Dsal.objects.get(dsal_filename=dsal_filename)

    def retrieve(self, request, dsal_filename):
        queryset = self.get_object(dsal_filename)
        serializer = DsalSerializer(queryset)
        dsal_content = codecs.decode(serializer.data['dsal_content'], 'unicode_escape')
        data_setting = convert_dsal_to_datasetting(dsal_content)
        import json
        with open('dataSetting.json', 'w') as outputfile:
            json.dump(data_setting, outputfile)
        return Response(data_setting)


class EmulationList(generics.ListCreateAPIView):
    # permission_classes = (IsAuthenticated,)

    def list(self, request):
        try:
            emulation_tasks = task_manager.get_all_tasks()
            serializer = EmulationSerializer(emulation_tasks, many=True)
            return Response(serializer.data)
        except ConnectionError as e:
            return Response({'error': 'Redis failed to connect.'})

    def create(self, request):
        new_task = task_manager.add_task_into_queue(request.data['task'])
        serializer = EmulationSerializer(new_task)
        return Response(serializer.data)


class EmulationItem(generics.RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, emulation_task_id):
        try:
            executing_task = task_manager.get_executing_task()
            executing_task['emulationStatus'] = task_manager.get_executing_task_status()
            serializer = EmulationSerializer(executing_task)
            return Response(serializer.data)
        except ConnectionError:
            return Response({'error': 'Redis failed to connect.'})

    def destroy(self, request, emulation_task_id):
        deleted_task = task_manager.delete_task(emulation_task_id)
        serializer = EmulationSerializer(deleted_task)
        return Response(serializer.data)


class ReportList(generics.ListAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticated,)


class ReportItem(generics.RetrieveDestroyAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticated,)


class ReportDownload(generics.RetrieveAPIView):
    queryset = Report.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_object(self, report_name):
        report_name = report_name.split('.')[0]
        return Report.objects.filter(report_name=report_name)

    def retrieve(self, request, report_file):
        report_name = report_file.split('.')[0]
        report_dataframe = pd.DataFrame(self.get_object(report_name).values())

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}'.format(report_file)

        report_dataframe.to_csv(path_or_buf=response)
        return response


class AvailableDeviceList(generics.RetrieveAPIView):
    # permission_classes = (IsAuthenticated,)

    def retrieve(self, request):
        try:
            available_device = task_manager.get_available_device()
            return Response({'availableDevice' :available_device})
        except ConnectionError:
            return Response({'error': 'Redis failed to connect.'})
