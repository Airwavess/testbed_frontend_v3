import json


def read_json(path: str):
    with open(path, "r") as inputfile:
        data = json.load(inputfile)
    return data


def save_json(path: str, data: dict):
    with open(path, "w") as outputfile:
        json.dump(data, outputfile)
