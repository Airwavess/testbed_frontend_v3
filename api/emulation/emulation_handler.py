from functools import wraps
import redis
from api.emulation import (
    Config,
    EmulationStatus
)


def abort_handled(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        redis_connection = redis.StrictRedis(
            host=Config.FRONTEND_IP, port=Config.REDIS_PORT, password=Config.REDIS_PASSWORD,
            encoding="utf-8", decode_responses=True)
        if redis_connection.get('emulation_status') == EmulationStatus.ABORT:
            return

        return fun(*args, **kwargs)
    return wrapper
