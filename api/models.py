from django.db import models


class Dsal(models.Model):
    dsal_filename = models.CharField(max_length=100, primary_key=True)
    dsal_content = models.TextField()
    dsal_updated = models.DateTimeField(auto_now=True)


class Report(models.Model):
    dsal = models.ForeignKey(Dsal, on_delete=models.CASCADE)
    report_name = models.CharField(max_length=100, primary_key=True)
    report_content = models.TextField()
    report_created = models.DateTimeField()
